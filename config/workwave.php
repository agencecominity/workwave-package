<?php

return [
    // Config for Guzzle client
    'http_client' => [
        'base_uri' => 'https://wwrm.workwave.com'
    ],
    // Authorization info
    'api_key' => env('WORKWAVE_API_KEY'),
    // Callback setup
    'callback' => [
        'url' => env('WORKWAVE_CALLBACK_URL','workwave.default-callback-url'), // Callback route name
        'signature_password' => env('WORKWAVE_CALLBACK__PASSWORD','IDidntChangeCallBackPasswordButIShouldDoIt'), // Password to verify workwave requests
        'test' => env('WORKWAVE_TEST_CALLBACK_URL',false), // Test given URL by workwave and set only if response was 200
        'handler' => '' // Class to handle workwave callback request. Should implement CallbackHandlerInterface.
    ],

];
