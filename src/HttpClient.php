<?php


namespace Cominity\WorkwavePackage;


use GuzzleHttp\Client;
use Illuminate\Support\Facades\Artisan;

class HttpClient
{
    private $client;

    /**
     * HttpClient constructor.
     */
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => config('workwave.http_client.base_uri'),
            'headers' => [
                'X-WorkWave-Key' => config('workwave.api_key')
            ]
        ]);
    }

    /**
     * Make request
     *
     * @param string $method
     * @param string $path
     * @param array|null $payload
     * @return mixed
     */
    public function request(string $method, string $path, ?array $payload = [])
    {
        return json_decode($this->client->request($method, $path, $payload)->getBody()->getContents(), 1);
    }

    /**
     * Get territories list
     *
     * @return mixed
     */
    public function listTerritories()
    {
        return $this->request('GET', '/api/v1/territories');
    }

    /**
     * Get list of regions by workwave territory id
     * @param string $territoryId
     * @return mixed
     */
    public function listRegions(string $territoryId)
    {
        return $this->request('GET','/api/v1/territories/'.$territoryId.'/regions');
    }

    /**
     * Get all orders from workwave
     * @return array
     */
    public function getAllOrders()
    {
        if (!WwTerritory::count()) {
            Artisan::call('workwave:getterritories');
        }
        $territories = WwTerritory::all();
        $orders = [];
        foreach ($territories as $territory) {
            $orderSet = $this->getTerritoryOrders($territory->ww_id);
            if (!empty($orderSet['orders'])) {
                foreach ($orderSet['orders'] as $order) {
                    $orders[] = $order;
                }
            }
        }
        return $orders;
    }

    /**
     * Get all orders of specific territory
     *
     * @param string $territoryId
     * @param array|null $ids
     * @return mixed
     */
    public function getTerritoryOrders(string $territoryId, ?array $ids = [])
    {
        return $this->request('GET', '/api/v1/territories/' . $territoryId . '/orders', ['json' => ['ids' => $ids]]);
    }

    /**
     * Create orders
     * @param string $territoryId
     * @param array $ordersSet
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function createOrders(string $territoryId, array $ordersSet)
    {
        return $this->request('POST', '/api/v1/territories/' . $territoryId . '/orders', ['json' => $ordersSet]);
    }

    /**
     * Get current callback URL from workwave
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getCallbackUrl()
    {
        return $this->request('GET', '/api/v1/callback');
    }

    /**
     * Set callback URL in workwave
     * @param string $url
     * @param string $signaturePassword
     * @param bool|null $test
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function setCallbackUrl(string $url, string $signaturePassword, ?bool $test = false)
    {
        return $this->request('POST', '/api/v1/callback', [
            'json' => [
                'url' => $url,
                'signaturePassword' => $signaturePassword,
                'test' => $test
            ]
        ]);
    }

    /**
     * Delete callback URL in workwave
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function deleteCallbackUrl()
    {
        return $this->request('DELETE', '/api/v1/callback');
    }

    /**
     * Delete all orders in all territories from workwave
     * @param array|null $territories
     */
    public function deleteAllOrders(?array $territories = null)
    {
        if (!$territories) {
            if (!WwTerritory::count()) {
                Artisan::call('workwave:getterritories');
            }
            $territories = WwTerritory::all()->pluck('ww_id')->toArray();
        }
        foreach ($territories as $territory) {
            $orders = [];
            $orderSet = $this->getTerritoryOrders($territory);
            if (!empty($orderSet['orders'])) {
                foreach ($orderSet['orders'] as $order) {
                    $orders[] = $order['id'];
                }
            }
            $this->deleteTerritoryOrders($territory, $orders);
        }
    }

    /**
     * Delete all orders in specific territory from workwave
     *
     * @param string $territoryId
     * @param array $orders
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function deleteTerritoryOrders(string $territoryId, array $orders)
    {
        return $this->request('DELETE', '/api/v1/territories/'.$territoryId.'/orders',['json' => ['ids' => $orders]]);
    }

    /**
     * Get all routes information from given territory
     *
     * @param string $territoryId
     * @return mixed
     */
    public function getTerritoryRoutes(string $territoryId)
    {
        return $this->request('GET','/api/v1/territories/'.$territoryId.'/toa/routes');
    }

    /**
     * Get all routes information from all territories
     *
     * @return array
     */
    public function getAllRoutes()
    {
        if (!WwTerritory::count()) {
            Artisan::call('workwave:getterritories');
        }
        $territories = WwTerritory::all();
        $routes = [
            'routes' => [],
            'orders' => [],
        ];
        foreach ($territories as $territory) {
            $routesSet = $this->getTerritoryRoutes($territory->ww_id);
            if (!empty($routesSet['routes'])) {
                foreach ($routesSet['routes'] as $route) {
                    $routes['routes'][] =  $route;
                }

            }
            if (!empty($routesSet['orders'])) {
                foreach ($routesSet['orders'] as $id=>$order) {
                    $routes['orders'][$id] =  $order;
                }
            }
        }
        return $routes;
    }

    /**
     * Get all orders with reported info attached
     *
     * @return array
     */
    public function getAllReportedInfo()
    {
        $orders = [];
        $routes = $this->getAllRoutes();
        foreach ($routes['routes'] as $route) {
            if (!empty($route['steps'])) {
                foreach ($route['steps'] as $step) {
                    if (!empty($step['orderId']) && !empty($step['trackingData'])) {
                        $order = $routes['orders'][$step['orderId']];
                        $order['reportedData'] = [
                            'status' => $step['trackingData']['status'],
                            'note' => (!empty($step['trackingData']['pods']['note']['text'])) ? $step['trackingData']['pods']['note']['text'] : ''
                        ];
                        $orders[] = $order;
                    }
                }
            }
        }
        return $orders;
    }

    /**
     * Replace data of given order
     *
     * @param string $territoryId
     * @param string $orderId
     * @param array $orderData
     * @return mixed
     */
    public function resetOrder(string $territoryId, string $orderId, array $orderData)
    {
        return $this->request('POST', '/api/v1/territories/' . $territoryId . '/orders/' . $orderId, ['json' => $orderData]);

    }
}
