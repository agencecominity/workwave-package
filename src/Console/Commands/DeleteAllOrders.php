<?php

namespace Cominity\WorkwavePackage\Console\Commands;

use Cominity\WorkwavePackage\HttpClient;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class DeleteAllOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'workwave:deleteallorders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove all orders from workwave';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param HttpClient $client
     * @return mixed
     */
    public function handle(HttpClient $client)
    {
        try {
            $response = $client->deleteAllOrders();
            echo 'ok'.PHP_EOL;
        } catch (\Exception $e) {
            Log::error('WARKWAVE :'.$e->getMessage());
        }
    }
}
