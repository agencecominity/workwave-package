<?php

namespace Cominity\WorkwavePackage\Console\Commands;

use Cominity\WorkwavePackage\HttpClient;
use Cominity\WorkwavePackage\WwTerritory;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class GetTerritories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'workwave:getterritories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get territories from workwave account and store in local DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param HttpClient $client
     * @return mixed
     */
    public function handle(HttpClient $client)
    {
        try {
            $territories = $client->listTerritories()['territories'];
            foreach ($territories as $territory) {
                WwTerritory::updateOrCreate(['ww_id' => $territory['id']], ['title' => $territory['name']]);
            }
            Log::info('WORKWAVE: territeries stored in local DB.');
        } catch (\Exception $e) {
            Log::error('WARKWAVE :'.$e->getMessage());
        }
    }
}
