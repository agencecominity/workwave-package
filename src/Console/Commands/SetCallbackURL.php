<?php

namespace Cominity\WorkwavePackage\Console\Commands;

use Cominity\WorkwavePackage\HttpClient;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SetCallbackURL extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'workwave:setcallbackurl';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set callback URL in workwave account according config file settings';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param HttpClient $client
     * @return mixed
     */
    public function handle(HttpClient $client)
    {
        try {
            $response = $client->setCallbackUrl(route(config('workwave.callback.url')), config('workwave.callback.signature_password'), config('workwave.callback.test'));
            echo $response['url'].PHP_EOL;
        } catch (\Exception $e) {
            Log::error('WARKWAVE :'.$e->getMessage());
        }
    }
}
