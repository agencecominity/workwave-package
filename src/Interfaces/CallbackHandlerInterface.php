<?php

namespace Cominity\WorkwavePackage\Interfaces;

interface CallbackHandlerInterface
{

    /**
     * Handle orders request according web app logic
     * @param string $eventType
     * @param array $request
     * @return mixed
     */
    public function handleOrdersRequest(string $eventType, array $request);

}
