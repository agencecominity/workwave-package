<?php

namespace Cominity\WorkwavePackage;

use Illuminate\Database\Eloquent\Model;

class WwTerritory extends Model
{
    protected $fillable = [
        'ww_id',
        'title'
    ];
}
