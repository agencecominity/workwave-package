<?php

namespace Cominity\WorkwavePackage\Http\Controllers;

use App\Http\Controllers\Controller;
use Cominity\WorkwavePackage\HttpClient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WorkwaveCallbackController extends Controller
{
    /**
     * Handle Workwave callback request
     * @param Request $request
     * @param HttpClient $httpClient
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle(Request $request, HttpClient $httpClient)
    {
        $rawUrl = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; //Laravel method orders query in alphabetical order and breaks hash
        $reqSignature = $request->get('signature');
        $fullUrl = str_replace('&signature='.$reqSignature,'',urldecode($rawUrl));
        $signature = base64_encode(hash_hmac('sha256', $fullUrl, config('workwave.callback.signature_password'), 1));
        if ($reqSignature != $signature) {
            Log::error('WORKWAVE : callback signature error. '.$reqSignature.' '.$signature.' '.$fullUrl);
            abort(403);
        }
        $handler = config('workwave.callback.handler') ;
        if (
            $handler &&
            class_exists($handler) &&
            in_array("Cominity\WorkwavePackage\Interfaces\CallbackHandlerInterface", class_implements($handler))
        ) {
            if ($request->get('event') == 'ordersChanged') {
                $territoryId = $request->get('territoryId');
                $created = !empty($request->get('data')['created']) ?
                    array_column($request->get('data')['created'],'id') :
                    $request->get('data')['created'];
                $updated = !empty($request->get('data')['updated']) ?
                    array_column($request->get('data')['updated'],'id') :
                    $request->get('data')['updated'];
                $deleted = $request->get('data')['deleted'];

                $handlerInstance = new $handler();
                if (!empty($deleted)) {
                    $handlerInstance->handleOrdersRequest('deleted',$deleted);
                }
                if (!empty($created)) {
                    $orders = $httpClient->getTerritoryOrders($territoryId,$created);
                    $handlerInstance->handleOrdersRequest('created',$orders);
                }
                if (!empty($updated)) {
                    $orders = $httpClient->getTerritoryOrders($territoryId,$updated);
                    $handlerInstance->handleOrdersRequest('updated',$orders);
                }

            }
        }
        return response()->json();
    }

    private function getDetails()
    {

    }
}
