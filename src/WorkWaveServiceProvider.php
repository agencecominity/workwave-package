<?php

namespace Cominity\WorkwavePackage;

use Cominity\WorkwavePackage\Console\Commands\DeleteAllOrders;
use Cominity\WorkwavePackage\Console\Commands\DeleteCallbackURL;
use Cominity\WorkwavePackage\Console\Commands\GetCallbackURL;
use Cominity\WorkwavePackage\Console\Commands\GetTerritories;
use Cominity\WorkwavePackage\Console\Commands\SetCallbackURL;
use Illuminate\Support\ServiceProvider;

class WorkWaveServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/workwave.php', 'workwave'
        );

        $this->app->bind(HttpClient::class, function ($app) {
            return new HttpClient();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/api.php');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->publishes([
            __DIR__.'/../config/workwave.php' => config_path('workwave.php'),
        ]);
        $this->commands([
            GetTerritories::class,
            GetCallbackURL::class,
            SetCallbackURL::class,
            DeleteCallbackURL::class,
            DeleteAllOrders::class
        ]);
    }
}
