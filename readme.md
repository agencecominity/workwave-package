Cominity Workwave API client

## About

This is Laravel 6.x package for interact with WorkWave API interface

## Install
1. Add `"cominity/workwave-package": "dev-master"` to composer.json.
2. Run `composer update`.
3. Run `php artisan:migrate`.
4. Run `php artisan vendor:publish` and choose `Cominity\WorkwavePackage\WorkWaveServiceProvider` option.
5. Create `WORKWAVE_API_KEY` env variable.
6. Run `php artisan workwave:getterritories`

## Usage

Package contains console commands:
```
workwave:getterritories - get all territories with workwave territory id and store it in local DB ( migration included )
workwave:getcallbackurl - returns webhook url currently set in workwave for current account
workwave:setcallbackurl - set webhook url in workwave for current account ( includes test request if config parameter was set )
workwave:deletecallbackurl - removes webhook url currently set in workwave for current account
workwave:deleteallorders - remove all orders in workwave for all territories exists in local DB 
```
Also there is pre-configured http client to interact with workwave api. It can be injected in any method and used like this:
```
use Cominity\WorkwavePackage\HttpClient;
...
public function someFunc(string $someVar, HttpClient $httpClient) 
{
...
$response = $httpClient->deleteAllOrders();
...
}
```

#### Client methods

request(string $method, string $path, ?array $payload = []) Make any desired request to workwave API

listTerritories() Get list of territories exisis in workwave for current account

getAllOrders() Get list of all orders for all territories from workwave

getTerritoryOrders(string $territoryId, ?array $ids = []) Get all orders for specific workwave territory id

createOrders(string $territoryId, array $ordersSet) Create orders set for specific territory id in workwave

getCallbackUrl() Get current webhook URL

setCallbackUrl(string $url, string $signaturePassword, ?bool $test = false) Set webhook URL for current account

deleteCallbackUrl() Delete webhook URL from workwave

deleteAllOrders() Delete all orders from all territories of current account from workwave

deleteTerritoryOrders(string $territoryId, array $orders) Delete listed orders from specific territory id

#### Webhook handling

There is set of callback settings in workwave.php config file:

```
'callback' => [
        'url' => env('WORKWAVE_CALLBACK_URL','workwave.default-callback-url'), // Callback route name
        'signature_password' => env('WORKWAVE_CALLBACK__PASSWORD','IDidntChangeCallBackPasswordButIShouldDoIt'), // Password to verify workwave requests
        'test' => env('WORKWAVE_TEST_CALLBACK_URL',false), // Test given URL by workwave and set only if response was 200
        'handler' => '' // Class to handle workwave callback request. Should implement CallbackHandlerInterface.
    ],
```  
You can create php class implementing Cominity\WorkwavePackage\Interfaces\CallbackHandlerInterface like this:

```
<?php


namespace App\Libs;


use Cominity\WorkwavePackage\Interfaces\CallbackHandlerInterface;
use Illuminate\Support\Facades\Log;

class WorkwaveCallbackHandler implements CallbackHandlerInterface
{

    /**
     * @param string $eventType
     * @param array $request
     */
    public function handleOrdersRequest(string $eventType, array $request)
    {
        Log::info($eventType.' '.print_r($request,1));
    }
}
```
And handle webhook request as needed. $eventType value will be one j 'created','updated','deleted' and $request will contain request related data array.

Also you can set custom 'url' in settings and use your own custom controller to handle raw workwave requests as you need.
